# Databricks notebook source
#a.Use the log_file as input
logfile=sc.textFile("FileStore/tables/log_file.txt")
header=logfile.first()
logfileRDD=logfile.filter(lambda x : x!=header)
logfileRDD.take(3)
logfileRDD1=logfileRDD.map(lambda x:x.replace("\"",""))
logfileRDD1.take(3)

# COMMAND ----------

#b.	Create a Scala hashmap with country codes and country names for all country codes available in the log_file
logfileRDD1=logfileRDD.map(lambda x:x.replace("\"",""))
logfileTokenRDD=logfileRDD1.map(lambda x:x.split(",")).filter(lambda x:(x[6]!="NA" and x[7]!="NA"))
countryCodes=logfileTokenRDD.map(lambda x:x[8]).distinct()
countryList=[('FR','France'),('CH','China'),('NL','Netherlands'),('DK','Denmark'),('DE','Germany'),('US','UnitedStates'),('AU','Australia'),('GB','Great Britain')]
countryDict=dict(countryList)

# COMMAND ----------

#c.	Perform a broadcast join to generate the below output using RDD API Country Code, Country Name, Total Downloads
countryListBC=sc.broadcast(countryDict)
totalDownloadsCountry=logfileTokenRDD.map(lambda x:(x[8],1)).reduceByKey(lambda x,y:x+y)
#totalDownloadsCountry.collect()
countryWiseDownloads=totalDownloadsCountry.map(lambda x:(x[0],countryListBC.value[x[0]],x[1]))
countryWiseDownloads.collect()

# COMMAND ----------

#d.	Save the output as a text file
countryWiseDownloads.map(lambda x:x[0]+","+x[1]+","+str(x[2])).saveAsTextFile("FileStore/tables/cwd")
