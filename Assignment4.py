# Databricks notebook source
#a.	Read the log_file and convert to a Data Frame and save as JSON File
logfile=sc.textFile("FileStore/table/log_file.txt")
from pyspark.sql.types import StructField, StringType, IntegerType, StructType
logschema=StructType().add("logdate",StringType()).add("logtime",StringType()).add("size",IntegerType()).add("r_version",StringType()).add("r_arch",StringType()).add("r_os",StringType()).add("dw_package",StringType()).add("ip_id",StringType()).add("countryCode",StringType())
df1=spark.read.format("csv").option("quote","\"").option("header","true").option("nullValue","NA").schema(logschema).load("FileStore/tables/log_file.txt")
js=df1.coalesce(1).write.format("json").save("FileStore/tables/logjson")

# COMMAND ----------

#b.	Read the JSON file into a data frame
js=StructType().add("logdate",StringType()).add("logtime",StringType()).add("size",IntegerType()).add("r_version",StringType()).add("r_arch",StringType()).add("r_os",StringType()).add("dw_package",StringType()).add("ip_id",StringType()).add("countryCode",StringType())
df4=spark.read.format("csv").option("quote","\"").option("header","true").option("nullValue","NA").schema(js).load("FileStore/tables/log_file.txt")

# COMMAND ----------

#c.	Perform the following transformations using Data Frame API
#c(a).	Group By Date
#(b).	Find Max, Min and Average Download Size for each Date

from pyspark.sql.functions import count,min,max,avg
groupDF=df1.groupBy("logdate").agg(max("size").alias("maxSize"),min("size").alias("minSize"),avg("size").alias("avgSize"))
groupDF.show(3)

# COMMAND ----------

#c(c).	Sort records by Date in Ascending Order
from pyspark.sql.functions import col
datecastDF=groupDF.withColumn("logdate1",(col("logdate").cast("date")))
sortDF=datecastDF.orderBy('logdate',ascending=False)
sortDF.show(3)

# COMMAND ----------

#c(d).	Save the output as a JSON file 
sortDF.coalesce(1).write.format("json").save("FileStore/tables/dsbc2")
#coalesce merges all partitions with minimum shuffle, but cannot increase the no of partitions

# COMMAND ----------

sortDF.repartition(1).write.format("json").save("FileStore/tables/dsbc3")
#Repartion does repartitioning by shuffling records, repartition can also be used to create more than the existing no of partitions
