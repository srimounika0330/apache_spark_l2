# Databricks notebook source
#a.	Read the log_file as a RDD
from pyspark.sql.types import StructField, StringType, IntegerType,StructType
logschema=StructType().add("logdate",StringType()).add("logtime",StringType()).add("size",IntegerType()).add("r_version",StringType()).add("r_arch",StringType()).add("r_os",StringType()).add("dw_package",StringType()).add("ip_id",StringType()).add("countryCode",StringType())

# COMMAND ----------

#b.	Convert the input RDD to a Data Frame
df1=spark.read.format("csv").option("quote","\"").option("header","true").option("nullValue","NA").schema(logschema).load("FileStore/tables/log_file.txt")
df1.show(3)

# COMMAND ----------

#c.	Add a new column to Data Frame called Download_Type
#If Size < 100,000, Download_Type = “Small”
#If Size > 100,000 and Size < 1,000,000, Download_Type = “Medium”
#Else Download_Type = “Large”

df2=df1.na.drop(subset=['dw_package'])
from pyspark.sql.functions import col,expr,when
df3=df2.withColumn("Download_type",when(col("size")<100000,"Small").when(col("size")<1000000,"Medium").otherwise("Large"))

# COMMAND ----------

#d.	Find the total number of Downloads by Country and Download Type
from pyspark.sql.functions import count,min,max,avg
downloadSizeByCountry=df3.groupBy('countryCode','Download_type').agg(count('Download_type').alias("Total_count"))
downloadSizeByCountry.show(5)

# COMMAND ----------

#e.	Save output as a Parquet File
downloadSizeByCountry.write.format('parquet').save("FileStore/tables/dsbc")
