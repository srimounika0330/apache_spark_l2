# Databricks notebook source
#a.Use the log_file as input
logfile=sc.textFile("FileStore/tables/log_file.txt")
header=logfile.first()
logfileRDD=logfile.filter(lambda x : x!=header)
logfileRDD.take(3)
logfileRDD1=logfileRDD.map(lambda x:x.replace("\"",""))
logfileRDD1.take(3)

# COMMAND ----------

#b.	Filter out records where package = “NA” and version = “NA”
logfileRDD1=logfileRDD.map(lambda x:x.replace("\"",""))
logfileTokenRDD=logfileRDD1.map(lambda x:x.split(",")).filter(lambda x:(x[6]!="NA" and x[7]!="NA"))
logfileTokenRDD.take(3)

# COMMAND ----------

#c.	Find total number of downloads for each package
totalDownloads=logfileTokenRDD.map(lambda x:(x[6],1)).reduceByKey(lambda x,y:x+y)
totalDownloads.take(4)

# COMMAND ----------

#d.	Find Average download size for each Country
downloadSumCount=logfileTokenRDD.map(lambda x:(x[8], int(x[2]))).aggregateByKey((0,0),lambda acc, value: (acc[0]+value,acc[1]+1),lambda acc,value:(acc[0]+value[0],acc[1]+value[1]))
avgDownloadsize=downloadSumCount.mapValues(lambda x:x[0]/x[1])
avgDownloadsize.collect()
